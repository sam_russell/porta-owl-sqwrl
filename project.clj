(defproject porta-owl "0.3.3-SNAPSHOT"
  :description "A DSL for Web Ontology Language (OWL) and SQWRL rule/query language."
  :url "https://gitlab.com/sam_russell/porta-owl-sqwrl"
  :license {:name "GPL-3.0"
            :url "https://www.gnu.org/licenses/gpl.txt"}
  :dependencies [[org.clojure/clojure "1.10.0"]
                 [edu.stanford.swrl/swrlapi "2.0.8"]
                 [edu.stanford.swrl/swrlapi-drools-engine "2.0.8"]
                 [org.clojars.stain/owlapi-jsonld "0.1.0"]
                 [cheshire "5.10.0"]
                 [tupelo "0.9.201"]]
  :repl-options {:init-ns porta-owl.core})

(ns porta-owl.core-test
  (:require [clojure.test :refer :all]
            [porta-owl.core :refer :all]
            [porta-owl.owl :as owl]
            [porta-owl.reader :as reader]))

;; TODO: make less ugly,
;; (see https://stackoverflow.com/questions/20053260/forward-declaring-a-var-from-another-namespace-in-clojure)

(deftest integration-test []
  (testing "Define ontology, make sqwrl engine, run query, get output"
    (is (=
         (.getIRI
          (:c (first
               (let [my-zoo (deref (defontology
                                     "http://sammysvirtualzoo.com/ontology#"
                                     "zoo"))]
                 (with-ontology zoo/ontology
                   (defentity critter [::owl/Class "critter"])
                   (defentity soda [::owl/NamedIndividual "soda"])
                   (defaxiom soda-critter (owl/axiom [::owl/Assertion
                                                      zoo/critter
                                                      zoo/soda])))
                 (result-rows ((sqwrl-query
                                (sqwrl-engine my-zoo)
                                "critter(?c) -> sqwrl:select(?c)")))))))
          (.getIRI (reader/read-owl [::owl/NamedIndividual "soda"]) )))))

(ns porta-owl.owl
  (:refer-clojure :exclude [class class?])
  (:require [clojure.string :as str])
  (:import
   (org.semanticweb.owlapi.model OWLOntologyManager OWLOntology
                                  IRI OWLOntologyIRIMapper
                                  OWLAxiom AxiomType
                                  OWLEntity OWLNamedObject
                                  OWLClass OWLClassExpression
                                  OWLAnnotation OWLAnnotationAssertionAxiom
                                  OWLLiteral
                                  OWLObjectProperty)
   (org.semanticweb.owlapi.vocab OWLFacet)
   (org.semanticweb.owlapi.apibinding OWLManager)
   (org.semanticweb.owlapi.search EntitySearcher))
  (:use [porta-owl.identifiers])
  (:use [porta-owl.datatypes])
  (:use [porta-owl.utils])
  (:use [porta-owl.data-factory])
  (:require [porta-owl.ontology-managers :as om])
  (:gen-class))


(defrecord Entity [iri])
(defn entity [iri-repr]
  "Processes different inputs into an Entity record"
  (->Entity iri-repr))
;;   (cond
;;     (string? iri-repr) (->Entity iri-repr)
;;     (keyword? iri-repr) (->Entity (kw->curie iri-repr))))

(defrecord Literal [datatype value])
(defn literal
  ([value] (case (map? value)
             true value
             false (->Literal (guess-type value) value)))
  ([datatype value] (->Literal (expand-owl-builtin datatype) value)))

(defrecord Annotation [property value])
(defn annotation
  ([property value] (->Annotation property value)))

(defrecord Axiom [signature annotations])
(defn axiom [signature & annotations]
  (->Axiom signature annotations))


(defn derive-entity-types []
  (let
      [entities
       {::Class [org.semanticweb.owlapi.model.OWLClassExpression]
        ::ObjectProperty [org.semanticweb.owlapi.model.OWLObjectPropertyExpression]
        ::DataProperty [org.semanticweb.owlapi.model.OWLDataPropertyExpression]
        ::Individual [org.semanticweb.owlapi.model.OWLIndividual]
        ::Literal [org.semanticweb.owlapi.model.OWLLiteral]
        ;; we treat a facet and a facet-restriction as if
        ;; they are the same thing.  basically.  this will
        ;; be addressed in more detail in the future.
        ::Facet [org.semanticweb.owlapi.vocab.OWLFacet
                 org.semanticweb.owlapi.model.OWLFacetRestriction]
        ::Datatype [org.semanticweb.owlapi.model.OWLDatatype
                    org.semanticweb.owlapi.model.OWLDataRange]}]
    ;;        ::Annotation 
    ;;        ::DataType}]
    (doseq [[k v] entities]
      (doseq [t v]
        (derive t k)))
    ;;    (doseq [t (reduce concat (vals entities))]
    (doseq [t (keys entities)]
      (derive t ::OWLObject))
    entities))


(defn derive-restriction-dispatch []
  (let [restrictions [::Existential
                      ::Universal
                      ::HasValue
                      ::HasSelf]]
    (doseq [r restrictions]
      (derive r ::Restriction))
    restrictions))

(defn derive-collection-exp-dispatch []
  (let [collection-exps [::Union
                         ::DisjointUnion
                         ::Intersection
                         ::Enumeration]]
    (doseq [a collection-exps]
      (derive a ::AggregateExpression))
    collection-exps))

(defn derive-collection-axiom-dispatch []
  (let [collection-axioms [::Equivalent
                           ::Disjoint
                           ::Same]]
    (doseq [a collection-axioms]
      (derive a ::AggregateAxiom))
    collection-axioms))


(defn derive-axiom-dispatch []
  (let [axioms [::Assertion
                ::SubElement
                ::SubClass
                ::SubProperty
                ::Declaration
                ::NegativeAssertion
                ::Functional
                ::Asymmetric
                ::Inverse
                ::InverseFunctional
                ::Irreflexive
                ::Reflexive
                ::Symmetric
                ::Transitive
                ::Range
                ::Domain]]
    (doseq [a axioms]
      (derive a ::Axiom))
    axioms))

;(defn derive-datatypes []
;  (doseq [t owl-builtin-types]
;    (let [[xmlns datatype] (str/split t (re-pattern ":"))]
;      (create-ns xmlns))))

(defn derive-facets []
  (let [facets [:xsd/minExclusive
                :xsd/fractionDigits
                :xsd/pattern
                :xsd/minLength
                :xsd/minInclusive
                :xsd/totalDigits
                :xsd/length
                :xsd/maxExclusive
                :xsd/maxLength
                :rdf/langRange
                :xsd/maxInclusive]]
    (doseq [f facets]
      (derive f ::Facet))
    facets))

(def restriction-dispatch (derive-restriction-dispatch))
(def axiom-dispatch (derive-axiom-dispatch))
(def entity-types (derive-entity-types))
(def aggregate-expression-dispatch (derive-collection-exp-dispatch))
(def aggregate-axiom-dispatch (derive-collection-axiom-dispatch))
(def datatype-dispatch (derive-owl-builtins))
(def facet-dispatch (derive-facets))

;; fun stuff
(def ∃ ::Existential)
(def ∀ ::Universal)
(def ∩ ::Intersection)
(def ∪ ::Union)
(def ∈ ::SubElement)

(ns porta-owl.datatypes
  (:refer-clojure :exclude [class class?])
  (:require [clojure.string :as str])
  (:require [tupelo.string])
  (:import
   (org.semanticweb.owlapi.model OWLOntologyManager OWLOntology
                                  IRI OWLOntologyIRIMapper
                                  OWLAxiom AxiomType
                                  OWLEntity OWLNamedObject
                                  OWLClass OWLClassExpression
                                  OWLAnnotation OWLAnnotationAssertionAxiom
                                  OWLLiteral
                                  OWLObjectProperty)
   (org.semanticweb.owlapi.vocab OWLFacet)
   (org.semanticweb.owlapi.apibinding OWLManager)
   (org.semanticweb.owlapi.search EntitySearcher))
  (:use [porta-owl.identifiers])
  (:gen-class))


(def owl-builtin-types
  #{"owl:backwardCompatibleWith"
    "owl:bottomDataProperty"
    "owl:bottomObjectProperty"
    "owl:deprecated"
    "owl:incompatibleWith"

    "owl:Nothing"
    "owl:priorVersion"
    "owl:rational"
    "owl:real"
    "owl:versionInfo"

    "owl:Thing"
    "owl:topDataProperty"
    "owl:topObjectProperty"
    "rdf:langRange"
    "rdf:PlainLiteral"

    "rdf:XMLLiteral"
    "rdfs:comment"
    "rdfs:isDefinedBy"
    "rdfs:label"
    "rdfs:Literal"

    "rdfs:seeAlso"
    "xsd:anyURI"
    "xsd:base64Binary"
    "xsd:boolean"
    "xsd:byte"

    "xsd:dateTime"
    "xsd:dateTimeStamp"
    "xsd:decimal"
    "xsd:double"
    "xsd:float"

    "xsd:hexBinary"
    "xsd:int"
    "xsd:integer"
    "xsd:language"
    "xsd:length"

    "xsd:long"
    "xsd:maxExclusive"
    "xsd:maxInclusive"
    "xsd:maxLength"
    "xsd:minExclusive"

    "xsd:minInclusive"
    "xsd:minLength"
    "xsd:Name"
    "xsd:NCName"
    "xsd:negativeInteger"

    "xsd:NMTOKEN"
    "xsd:nonNegativeInteger"
    "xsd:nonPositiveInteger"
    "xsd:normalizedString"
    "xsd:pattern"

    "xsd:positiveInteger"
    "xsd:short"
    "xsd:string"
    "xsd:token"
    "xsd:unsignedByte"

    "xsd:unsignedInt"
    "xsd:unsignedLong"
    "xsd:unsignedShort"})


(defn derive-owl-builtins []
  (doseq [[xmlns t] (map
              #(str/split % (re-pattern ":"))
              owl-builtin-types)]
    (derive (keyword xmlns t) ::BuiltIn)))


;; uh... this doesn't seem to be limited to the owl-builtins?
;; not a problem, but the function should be refactored as a more
;; general keyword to CURIE string utilty func
(defn expand-owl-builtin [builtin-kw & [onto]]
  (cond
    (string? builtin-kw) (full-iri builtin-kw onto)
    (keyword
     builtin-kw) (let [s (str builtin-kw)]
                   (-> s
                       (str/replace-first (re-pattern ":") "")
                       (str/replace-first (re-pattern "/") ":")
                       (full-iri onto)))))

(def owl-types {:bool "xsd:boolean"
                :int "xsd:integer"
                :double "xsd:double"
                :float "xsd:float"
                :str "xsd:string"})

;; TODO: make good type-guesser
(def java-types {java.lang.Boolean "xsd:boolean" 
                 java.lang.Long "xsd:int" 
                 java.lang.Double "xsd:float" 
                 java.lang.String "xsd:string"})


;; not sure why the import doesn't work, but it doesn't!
(defn facet-from-symbolic [sym-name]
  (OWLFacet/getFacetBySymbolicName sym-name))

(defn intern-owl-facets [ns-sym]
  (map
   #(intern ns-sym (symbol %)
            (curie
             (.toString (.getIRI (facet-from-symbolic %)))))
   ["length"
    "minLength"
    "maxLength"
    "pattern"
    ">="
    ">"
    "<="
    "<"
    "totalDigits"
    "fractionDigits"
    "langRange"]))
   

(defn guess-type [x]
  (let [t (java-types (type x))]
    (if t
      t
      :literal)))


(defn safe-entity [entity]
  (cond
    (keyword? entity) (expand-owl-builtin entity)
    :else entity))

(defn safe-builtin [b]
  (cond
    (keyword? b) (expand-owl-builtin b)
    :else b))

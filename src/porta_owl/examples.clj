(ns porta-owl.examples
  (:require [porta-owl.core :refer :all]
            [porta-owl.owl :as owl]
            [porta-owl.ontology-managers :as om]
            [porta-owl.reader :as reader])
  (:use [porta-owl.identifiers])
  (:gen-class))

(defontology "http://sammysvirtualzoo.com/ontology#" "homepage")
  
(prefix-map-update!
 homepage/ontology
 {"bf:" "http://id.loc.gov/ontologies/bibframe/"
  "aat:" "http://vocab.getty.edu/aat/"
  "madsrdf:" "http://www.loc.gov/mads/rdf/v1#"})

(defn load-madsrdf []
  (.loadOntology
   (om/ontology-manager homepage/ontology)
   (make-iri (get (prefix-map homepage/ontology) "madsrdf:"))))

(def imports (om/ontology-import
              homepage/ontology
              (get (prefix-map homepage/ontology) "madsrdf:")))

(with-ontology homepage
  (defentity asset [::owl/Class "asset"])
  (defentity has-tagline [::owl/DataProperty "has_tagline"])
  (defentity gb-porta [::owl/NamedIndividual "gb_porta"]))

(with-ontology homepage
  (defentity porta-portrait [::owl/NamedIndividual "porta_portrait"]
    (owl/annotation
     :rdfs/label "Portrait of Giambattista della Porta"))
  (defaxiom porta-genre
    (owl/axiom [::owl/Assertion
                [::owl/Class "aat:300015637"] homepage/porta-portrait]))
  (defaxiom portrait-asset (owl/axiom [::owl/Assertion
                                       homepage/asset
                                       homepage/porta-portrait]))
  (defaxiom portrait-tagline
    (owl/axiom [::owl/Assertion
                homepage/has-tagline
                homepage/porta-portrait
                (owl/literal "Buon giorno, amici!!")]))
  (defaxiom porta-authority
    (owl/axiom [::owl/Same
                homepage/gb-porta
                [::owl/NamedIndividual
                 "http://id.loc.gov/authorities/names/n79038538"]]))
  (defaxiom portrait-subject
    (owl/axiom [::owl/Assertion
                [::owl/ObjectProperty "bf:Subject"]
                homepage/porta-portrait
                homepage/gb-porta])))

(def sqwrl (sqwrl-engine homepage/ontology))

(defn test-query []
  (sqwrl-query sqwrl 'homepage "q1" "asset(?a) -> sqwrl:select(?a)"))

;; (.applyChange (om/ontology-manager homepage/ontology) (AddImport. homepage/ontology (.getOWLImportsDeclaration fac/fac (make-iri (get (prefix-map madsrdf) ":")))))


(defontology "http://sammysvirtualzoo/ontology#" "zoo")

;; declarations
(with-ontology zoo
  (defentity critter [::owl/Class "critter"])
  (defentity pet [::owl/Class "pet"])
  (defentity person [::owl/Class "person"])
  (defentity cares_for [::owl/ObjectProperty "cares_for"])
  (defentity cared_for_by [::owl/ObjectProperty "cared_for_by"])
  (defentity soda [::owl/NamedIndividual "soda"]
    (owl/annotation :rdfs/label "Soda Hobart"))
  (defentity sam [::owl/NamedIndividual "sam"]
    (owl/annotation :rdfs/label "Sam")))

;; t-box axioms
(with-ontology zoo
  (defaxiom cares-inverse
    (owl/axiom [::owl/Inverse zoo/cares_for zoo/cared_for_by]))
  (defaxiom cares-for-domain
    (owl/axiom [::owl/Domain zoo/cares_for zoo/person]))
  (defaxiom pet-axiom
    (owl/axiom [::owl/Equivalent
                zoo/pet
                [::owl/Intersection
                 zoo/critter
                 [::owl/Existential zoo/cared_for_by zoo/person]]])))

;; a-box axioms
(with-ontology zoo
  (defaxiom soda-critter
    (owl/axiom [::owl/Assertion zoo/critter zoo/soda]))
  (defaxiom soda-pet
    (owl/axiom [::owl/Assertion zoo/cared_for_by zoo/soda zoo/sam]))
  (defaxiom sam-person
    (owl/axiom [::owl/Assertion zoo/person zoo/sam])))


(def rules (rule-engine zoo/ontology))

(def is-pet-sqwrl "ispet" "pet(?p) -> sqwrl:select(?p)")

(defn pet-rule [engine]
  (.createSWRLRule
   engine "is-pet-rule"
   "person(?p) ^ critter(?c) ^ cares_for(?p, ?c) -> pet(?c)"))


(defn axiom-types [engine]
  (distinct (map (fn [x] (.toString (.getAxiomType x)))
                 (asserted-axioms engine))))

(defn comp-axioms [engine]
  (let [t (axiom-types engine)]
    (reduce #(assoc %1 (keyword %2)
                    {:inferred (filter
                                (fn [x] (= (.toString
                                            (.getAxiomType x))
                                           %2))
                                (inferred-axioms engine))
                     :asserted (filter
                                (fn [x] (= (.toString
                                            (.getAxiomType x))
                                           %2))
                                (asserted-axioms engine))})
            {} t)))

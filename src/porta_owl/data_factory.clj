(ns porta-owl.data-factory
  (:refer-clojure :exclude [class class?])
  (:import
   (org.semanticweb.owlapi.model OWLOntologyManager OWLOntology
                                  IRI OWLOntologyIRIMapper
                                  OWLAxiom AxiomType
                                  OWLEntity OWLNamedObject
                                  OWLClass OWLClassExpression
                                  OWLAnnotation OWLAnnotationAssertionAxiom
                                  OWLLiteral
                                  OWLObjectProperty)
   (org.semanticweb.owlapi.vocab OWLFacet)
   (org.semanticweb.owlapi.apibinding OWLManager)
   (org.semanticweb.owlapi.search EntitySearcher))
  (:use [porta-owl.identifiers])
  (:use [porta-owl.datatypes])
  (:use [porta-owl.utils])
  (:require [porta-owl.ontology-managers :as om])
  (:gen-class))

(def fac (om/data-factory))


;;; Basic entities
(defn make-class
  ([iri-str] (.getOWLClass fac (IRI/create iri-str)))
  ([onto iri-str]
   (print (expand onto iri-str) (newline))
   (.getOWLClass
    fac
    (make-iri (expand onto iri-str)))))

(defn make-obj-prop
  ([iri-str] (.getOWLObjectProperty fac (IRI/create iri-str)))
  ([onto iri-str] (.getOWLObjectProperty
                   fac
                   (make-iri (expand onto iri-str)))))
  
(defn make-data-prop
  ([iri-str] (.getOWLDataProperty fac (IRI/create iri-str)))
  ([onto iri-str] (.getOWLDataProperty
                   fac
                   (make-iri (expand onto iri-str)))))
  
(defn make-data-type
  ([iri-str] (.getOWLDatatype fac (IRI/create iri-str)))
  ([onto iri-str] (.getOWLDatatype
                   fac
                   (make-iri (expand onto iri-str)))))
  
(defn make-individual
  ([iri-str] (.getOWLNamedIndividual fac (IRI/create iri-str)))
  ([onto iri-str] (.getOWLNamedIndividual
                   fac
                   (make-iri (expand onto iri-str)))))
  
(defn make-literal [t val]
  (.getOWLLiteral fac (str val) (expand-owl-builtin t)))
(defn make-facet [facet-kw & _]
  (OWLFacet/getFacet (make-iri (expand-owl-builtin facet-kw))))


;;; Annotations
(defn make-label [str-val] (.getRDFSLabel fac str-val))
(defn make-comment [str-val] (.getRDFSComment fac str-val))
(defn make-annotation-property [prop]
  (.getOWLAnnotationProperty fac prop))
(defn make-annotation [property value]
  (.getOWLAnnotation fac property value))
(defn make-annotation-assertion [subject annotation]
  (.getOWLAnnotationAssertionAxiom fac subject annotation))

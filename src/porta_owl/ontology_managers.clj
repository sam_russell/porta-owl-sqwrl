(ns porta-owl.ontology-managers
  (:refer-clojure :exclude [class class?])
  (:require [clojure.java.io :as io])
  (:require [clojure.string :as str])
  (:require [tupelo.string])
  (:import
   (org.semanticweb.owlapi.model OWLOntologyManager OWLOntology
                                 AddImport
                                 IRI OWLOntologyIRIMapper
                                 OWLAxiom AxiomType
                                 OWLEntity OWLNamedObject
                                 OWLClass OWLClassExpression
                                 OWLAnnotation OWLAnnotationAssertionAxiom
                                 OWLLiteral
                                 OWLObjectProperty)
   (org.semanticweb.owlapi.util OWLOntologyMerger)
   (org.semanticweb.owlapi.vocab OWLFacet)
   (org.semanticweb.owlapi.apibinding OWLManager)
   (org.semanticweb.owlapi.search EntitySearcher))
  (:use [porta-owl.identifiers])
  (:use [porta-owl.datatypes])
  (:use [porta-owl.utils])
  (:gen-class))


(defn data-factory [] (OWLManager/getOWLDataFactory))

(defn manager [& args]
  "Creates an OWLOntologyManager"
  (OWLManager/createOWLOntologyManager))

(defn ontology-manager [onto]
  (.getOWLOntologyManager onto))

;; (.applyChange (om/ontology-manager homepage/ontology) (AddImport. homepage/ontology (.getOWLImportsDeclaration fac/fac (make-iri (get (prefix-map madsrdf) ":")))))

(defrecord OntologyID [iri version])
(defn make-ontology-id [iri version]
  (->OntologyID iri version))

(defn get-ontology-version-id [onto]
  (let [onto-id (.getOntologyID onto)]
      (second
       (re-find #"\((.+)\)"
                (.toString (.getVersionIRI onto-id))))))

(defn ontology-import [onto & imports]
  "import n... ontologies by IRI"
  (let [mgr (ontology-manager onto)
        fac (data-factory)]
    (doseq [i imports]
      (.applyChange
       mgr (AddImport. onto (.getOWLImportsDeclaration
                             fac
                             (make-iri i)))))))


(defn ontology-import-load [onto imports-prefix-map]
  (let [mgr (ontology-manager onto)
        fac (data-factory)]
    (apply ontology-import
           onto
           (map
            (fn [i]
              (do
                (.loadOntology mgr (make-iri i))
                i))
            (vals imports-prefix-map)))
    (prefix-map-update! onto imports-prefix-map)
    (seq (.getImportsClosure onto))))
                                   

(defn merge-ontologies [o1 o2]
  (let* [manager (ontology-manager o1)
         merger (OWLOntologyMerger. manager)]
    (.createMergedOntology merger manager (make-iri
                                           (get (prefix-map o2) ":")))))

(defn create-ontology
  ([iri]
   (.createOntology (manager) (make-iri iri)))
  ([iri manager-fn] (.createOntology (manager-fn) (make-iri iri))))

(defn ontology-default-prefix-set! [onto prefix]
  "Sets default prefix for ontology"
  (.setDefaultPrefix (ontology-format onto) prefix))

(defn ontology-default-prefix [onto]
  (.getDefaultPrefix (ontology-format onto)))

(defn create-ontology-broken [& {:keys [iri
                                 axioms
                                 direct-imports
                                        manager-args]}]
  "Creates a new ontology with optional IRI, axioms, imports, and manager settings"
  (let [mgr (manager manager-args)]
    (.createOntology mgr iri axioms direct-imports)))

(defn load-ontology-document
  ([filepath]
   (.loadOntologyFromOntologyDocument (manager) (io/file filepath)))
  ([filepath mgr]
   (.loadOntologyFromOntologyDocument mgr (io/file filepath))))

(defn save-ontology [onto filepath]
  (let [mgr (ontology-manager onto)]
    (with-open [out-s (io/output-stream filepath)]
      (.saveOntology mgr onto out-s))))

(defn add-axioms! [onto axioms]
  (.addAxioms (manager) onto (java.util.Set/copyOf axioms)))

(defn labels [onto subject]
  (let* [subject (.getIRI subject)
         atn-axioms (.getAnnotationAssertionAxioms onto subject)]
    (filter
     #(not (nil? %))
     (map (fn [atn-axiom]
            (let [atn (.getAnnotation atn-axiom)]
              (if (.isLabel (.getProperty atn))
                (.getValue atn))))
          atn-axioms))))

(defn get-labels [owl-entity onto]
  (let [label (.getOWLAnnotationProperty
               (data-factory) (expand-owl-builtin :rdfs/label))]
    (EntitySearcher/getAnnotations
             owl-entity
             onto
             label)))

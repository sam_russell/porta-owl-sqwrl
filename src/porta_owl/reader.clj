(ns porta-owl.reader
  (:refer-clojure :exclude [class class?])
  (:require [clojure.string :as str])
  (:require [tupelo.string])
  (:import
   (org.semanticweb.owlapi.model OWLOntologyManager OWLOntology
                                  IRI OWLOntologyIRIMapper
                                  OWLAxiom AxiomType
                                  OWLEntity OWLNamedObject
                                  OWLClass OWLClassExpression
                                  OWLAnnotation OWLAnnotationAssertionAxiom
                                  OWLLiteral
                                  OWLObjectProperty)
   (org.semanticweb.owlapi.vocab OWLFacet)
   (org.semanticweb.owlapi.apibinding OWLManager)
   (org.semanticweb.owlapi.search EntitySearcher))
  (:use [porta-owl.identifiers])
  (:use [porta-owl.datatypes])
  (:use [porta-owl.utils])
  (:require [porta-owl.owl :as owl])
  (:use [porta-owl.data-factory])
  (:gen-class))


;;; OWLAPI constructor dispatching functions
;;; These multimethods allow the Porta-Owl language to offer
;;; a greater degree of abstraction than other OWL syntaxes.

(defmulti type-dispatch
  (fn [function owl-object]
    [function
     (let [owl-obj-type (filter
             #(not (nil? (::owl/OWLObject (parents %))))
             (ancestors (type owl-object)))]
       (if (empty? owl-obj-type)
         nil
         (first owl-obj-type)))]))

;;; Data Property Restrictions
(defmethod type-dispatch [::owl/Existential ::owl/DataProperty]
  [function prop]
  (fn [i j] (.getOWLDataSomeValuesFrom fac i j)))

(defmethod type-dispatch [::owl/Universal ::owl/DataProperty]
  [function prop]
  (fn [i j] (.getOWLDataAllValuesFrom fac i j)))

(defmethod type-dispatch [::owl/Value ::owl/DataProperty]
  [function prop]
  (fn [i j] (.getOWLDataHasValue fac i j)))

;;; TODO: add remaining restrictions, particularyly DataRange and DataTypeRestriction

;;; Object Property Restrictions
(defmethod type-dispatch [::owl/Existential ::owl/ObjectProperty]
  [function prop]
  (fn [i j] (.getOWLObjectSomeValuesFrom fac i j)))

(defmethod type-dispatch [::owl/Universal ::owl/ObjectProperty]
  [function prop]
  (fn [i j] (.getOWLObjectAllValuesFrom fac i j)))

(defmethod type-dispatch [::owl/Value ::owl/ObjectProperty]
  [function prop]
  (fn [i j] (.getOWLObjectHasValue fac i j)))

(defmethod type-dispatch [::owl/Self ::owl/ObjectProperty]
  [function prop]
  (fn [i j] (.getOWLObjectHasSelf fac i j)))


;; Facet and Data Type Restrictions
(defmethod type-dispatch [::owl/DatatypeRestriction
                          ::owl/Datatype]
  [function facet-restriction]
  (fn [i j] (.getOWLDatatypeRestriction fac i j)))


;;; Boolean connectives

(defmethod type-dispatch [::owl/Intersection ::owl/Class]
  [function owl-object]
  (fn [i] (.getOWLObjectIntersectionOf fac i)))

;; TODO: DataType is wrong! Must implement DataRange
(defmethod type-dispatch [::owl/Intersection ::owl/Datatype]
  [function owl-object]
  (fn [i] (.getOWLDataIntersectionOf fac i)))

(defmethod type-dispatch [::owl/Union ::owl/Class]
  [function owl-object]
  (fn [i] (.getOWLObjectUnionOf fac i)))

(defmethod type-dispatch [::owl/Union ::owl/Datatype]
  [function owl-object]
  (fn [i] (.getOWLDataUnionOf fac i)))


;;; Axioms

(defmethod type-dispatch [::owl/Assertion ::owl/Class]
  [function owl-object]
  (fn [i j] (.getOWLClassAssertionAxiom fac i j)))

(defmethod type-dispatch [::owl/Assertion ::owl/ObjectProperty]
  [function owl-object]
  (fn [i j k] (.getOWLObjectPropertyAssertionAxiom fac i j k)))

(defmethod type-dispatch [::owl/Assertion ::owl/DataProperty]
  [function owl-object]
  (fn [i j k] (.getOWLDataPropertyAssertionAxiom fac i j k)))

(defmethod type-dispatch [::owl/Functional ::owl/DataProperty]
  [function owl-object]
  (fn [i] (.getOWLFunctionalDataPropertyAxiom fac i)))

(defmethod type-dispatch [::owl/Functional ::owl/ObjectProperty]
  [function owl-object]
  (fn [i] (.getOWLFunctionalObjectPropertyAxiom fac i)))

(defmethod type-dispatch [::owl/SubElement ::owl/Class]
  [function owl-object]
  (fn [i j] (.getOWLSubClassOfAxiom fac i j)))

(defmethod type-dispatch [::owl/SubElement ::owl/ObjectProperty]
  [function owl-object]
  (fn [i j] (.getOWLSubObjectPropertyOfAxiom fac i j)))

(defmethod type-dispatch [::owl/SubElement ::owl/DataProperty]
  [function owl-object]
  (fn [i j] (.getOWLSubDataPropertyOfAxiom fac i j)))

(defmethod type-dispatch [::owl/SubClass ::owl/Class]
  [function owl-object]
  (fn [i j] (.getOWLSubClassOfAxiom fac i j)))

(defmethod type-dispatch [::owl/SubProperty ::owl/ObjectProperty]
  [function owl-object]
  (fn [i j] (.getOWLSubObjectPropertyOfAxiom fac i j)))

(defmethod type-dispatch [::owl/SubProperty ::owl/DataProperty]
  [function owl-object]
  (fn [i j] (.getOWLSubDataPropertyOfAxiom fac i j)))

;; Aggregate-accepting axioms

(defmethod type-dispatch [::owl/Equivalent ::owl/Class]
  [function owl-object]
  (fn [i] (.getOWLEquivalentClassesAxiom fac i)))

(defmethod type-dispatch [::owl/Equivalent ::owl/ObjectProperty]
  [function owl-object]
  (fn [i] (.getOWLEquivalentObjectPropertiesAxiom fac i)))

(defmethod type-dispatch [::owl/Equivalent ::owl/DataProperty]
  [function owl-object]
  (fn [i] (.getOWLEquivalentDataPropertiesAxiom fac i)))

(defmethod type-dispatch [::owl/Disjoint ::owl/Class]
  [function owl-object]
  (fn [i] (.getOWLDisjointClassesAxiom fac i)))

(defmethod type-dispatch [::owl/Disjoint ::owl/ObjectProperty]
  [function owl-object]
  (fn [i] (.getOWLDisjointObjectPropertiesAxiom fac i)))

(defmethod type-dispatch [::owl/Disjoint ::owl/DataProperty]
  [function owl-object]
  (fn [i] (.getOWLDisjointDataPropertiesAxiom fac i)))

(defmethod type-dispatch [::owl/Range ::owl/ObjectProperty]
  [function owl-object]
  (fn [i j] (.getOWLObjectPropertyRangeAxiom fac i j)))

(defmethod type-dispatch [::owl/Range ::owl/DataProperty]
  [function owl-object]
  (fn [i j] (.getOWLDataPropertyRangeAxiom fac i j)))

(defmethod type-dispatch [::owl/Domain ::owl/ObjectProperty]
  [function owl-object]
  (fn [i j] (.getOWLObjectPropertyDomainAxiom fac i j)))

(defmethod type-dispatch [::owl/Domain ::owl/DataProperty]
  [function owl-object]
  (fn [i j] (.getOWLDataPropertyDomainAxiom fac i j)))

;;; Object property axioms

(defmethod type-dispatch [::owl/Asymmetric ::owl/ObjectProperty]
  [function owl-object]
  (fn [i j] (.getOWLAsymmetricObjectPropertyAxiom fac i j)))

(defmethod type-dispatch [::owl/Functional ::owl/ObjectProperty]
  [function owl-object]
  (fn [i] (.getOWLFunctionalDataPropertyAxiom fac i)))

(defmethod type-dispatch [::owl/Inverse
                          ::owl/ObjectProperty]
  [function owl-object]
  (fn [i j]
    (.getOWLInverseObjectPropertiesAxiom fac i j)))

(defmethod type-dispatch [::owl/InverseFunctional
                          ::owl/ObjectProperty]
  [function owl-object]
  (fn [i j]
    (.getOWLInverseFunctionalObjectPropertyAxiom fac i j)))

(defmethod type-dispatch [::owl/Irreflexive ::owl/ObjectProperty]
  [function owl-object]
  (fn [i j]
    (.getOWLIrreflexiveObjectPropertyAxiom fac i j)))

(defmethod type-dispatch [::owl/Reflexive ::owl/ObjectProperty]
  [function owl-object]
  (fn [i j]
    (.getOWLReflexiveObjectPropertyAxiom fac i j)))

(defmethod type-dispatch [::owl/Symmetric ::owl/ObjectProperty]
  [function owl-object]
  (fn [i j] (.getOWLSymmetricObjectPropertyAxiom fac i j)))

(defmethod type-dispatch [::owl/Transitive ::owl/ObjectProperty]
  [function owl-object]
  (fn [i j] (.getOWLTransitiveObjectPropertyAxiom fac i j)))

;;; Individual Axioms

(defmethod type-dispatch [::owl/Same ::owl/Individual]
  [function owl-object]
  (fn [i] (.getOWLSameIndividualAxiom fac i)))

(defmethod type-dispatch [::owl/Different ::owl/Individual]
  [function owl-object]
  (fn [i] (.getOWLDifferentIndividualAxiom fac i)))

(defmethod type-dispatch [::owl/Same ::owl/NamedIndividual]
  [function owl-object]
  (fn [i] (.getOWLSameIndividualAxiom fac i)))

(defmethod type-dispatch [::owl/Different ::owl/NamedIndividual]
  [function owl-object]
  (fn [i] (.getOWLDifferentIndividualAxiom fac i)))

;; not actually used
(defmethod type-dispatch [::owl/Facet ::owl/Literal]
  [function owl-object]
  (fn [i j] (.getOWLFacetRestriction fac i j)))

(defmethod type-dispatch [::owl/Declaration ::owl/Datatype]
  [function owl-object]
  (fn [i j] (.getOWLDatatypeDefinitionAxiom fac i j)))

(defmethod type-dispatch [::owl/Declaration ::owl/OWLObject]
  [function owl-object]
  (fn [i] (.getOWLDeclarationAxiom fac i)))

(defn read-entity [e ctor-fn]
  "Reads entities represented as kw, string, or Entity defrecord"
  ;; What is the purpose of using the defrecord here?
  ;; None right now, apparently.  The defrecord doesn't implement
  ;; any protocols, so I think that an ordinary map could be used
  ;; instead.
  (cond
    (instance? porta_owl.owl.Entity e) (ctor-fn (:iri e))
    (instance? porta_owl.owl.Literal e) (ctor-fn
                                         (:datatype e)
                                         (:value e))
    :else (ctor-fn (:iri (owl/entity e)))))

(defmulti read-expression
  "Reads any type of expression"
  (fn [exp & [onto]] (cond
                       (coll? exp) (first exp)
                       :else exp)))


(defn exp-expand [exp]
  "Expands a collection of OWL expressions in an OWL expression body"
  (reduce #(if (not (coll? %1))
             (into %1 %2)
             (reduced exp)) (rest exp)))


(defn exp-xf [map-fn]
  "Returns an xform for OWL expressions"
  (let [xf (comp
            (filter #(not (keyword? %)))
            (map map-fn))]
    xf))

(defn exp-t
  "OWL expression transformer with composed mappers and reducers"
  ([exp dispatch-fn]
  (dispatch-fn
   (first exp)
   ;; could make the xform an optional parameter where
   ;; the default is the exp-xf as seen here
   (transduce (exp-xf #(list (read-expression %)))
              into []
              (exp-expand exp))))
  ;; second parameter signature uses prefix manager for making IRIs
  ([exp dispatch-fn onto]
   (dispatch-fn
    (first exp)
    ;; could make the xform an optional parameter where
    ;; the default is the exp-xf as seen here
    (transduce (exp-xf #(list (read-expression % onto)))
               into []
               (exp-expand exp)))))


(defn literal? [maybe-lit]
  "Anything that is an appropriate map, or NOT a collection 
may be regarded as a literal."
  (cond
    ;; can't be a map unless it matches the literal defrecord
    (map? maybe-lit) (if (= (set (keys maybe-lit)) #{:datatype :value})
                       true
                       false)
    (and
     ;;can't be an OWL Object
     (not (isa? (type maybe-lit) ::owl/OWLObject))
     ;; can't be a collection
     (not (coll? maybe-lit))
     ;; can't be a keyword
     (not (keyword? maybe-lit))) true
    :else false))

(defn facet? [maybe-f]
  "Predicate for membership in a special subset of data types"
  (isa? maybe-f ::owl/Facet))

(defn axiom? [maybe-a]
  "Predicate for if something is an axiom"
  nil)

(defn read-literal [lit]
  (cond
    (map? lit) (read-entity
                (owl/literal (:datatype lit) (:value lit))
                make-literal)
    :else (read-entity (owl/literal lit) make-literal)))

;;; defaults to entity
(defmethod read-expression :default [exp & [onto]]
  (let [ctors-lookup
        (cond
          (nil? onto) {::owl/Class make-class
                       ::owl/ObjectProperty make-obj-prop
                       ::owl/DataProperty make-data-prop
                       ::owl/Datatype make-data-type
                       ::owl/NamedIndividual make-individual}
          :else {::owl/Class (partial make-class onto)
                 ::owl/ObjectProperty (partial make-obj-prop onto)
                 ::owl/DataProperty (partial make-data-prop onto)
                 ::owl/Datatype (partial make-data-type onto)
                 ::owl/NamedIndividual (partial make-individual onto)})]
    (cond
      (literal? exp) (read-literal exp)
      :else (read-entity (second exp) ((first exp) ctors-lookup)))))

  
(defmethod read-expression ::owl/Restriction
  ([exp]
  (exp-t exp (fn [f args]
               (apply
                (type-dispatch f (first args))
                args))))
  ([exp onto]
   (exp-t exp (fn [f args]
                    (apply
                     (type-dispatch f (first args))
                     args))
          onto)))

(defmethod read-expression ::owl/Axiom
  ([exp]
  (exp-t
   ;; For a unary declaration axiom, you have to put your entity
   ;; into a collection type, otherwise the keywords are stripped
   ;; from the head of the list and the entity is assumed to be an
   ;; OWLLiteral, which is not a valid argument for a declaration
   ;; axiom.  In hindsight, I ought to have made everything be
   ;; contained in a list, even if it's an empty list or 1-item list,
   ;; because then I wouldn't have these put-scalar-in-list functions
   ;; all over the place to make the composed pipelines/transducers
   ;; work.  But, maybe it is a necessary evil to accomodate the use
   ;; of both scalar literals and collections types as arguments?
   
   ;; Also, these types of unary functions seem to be edge-cases,
   ;; I think ::owl/Declaration and maybe 1 or 2 others (::owl/HasSelf ?)
   ;; might be the extent of these troublesome ones... 
   (cond
     (= 1 (count (rest exp))) [(first exp) (rest exp)]
     :else exp)
   (fn [f args]
     (apply
      (type-dispatch f (first args))
      args))))
  ;; including prefix manager
  ([exp onto]
   (exp-t (cond
            (= 1 (count (rest exp))) [(first exp) (rest exp)]
            :else exp)
          (fn [f args] (apply
                        (type-dispatch f (first args)) args))
          onto)))


;;; There is some redundancy between the handling of ::owl/AggregateExpression
;;; and ::owl/AggregateAxiom OWL functions, but additional constraints will
;;; likely be added to these multimethods to reflect the different OWL
;;; profiles.  For example, OWL RL (or is it QL?) doesn't allow for
;;; certain expressions or quantifiers for the superclass parameter of
;;; a `SubClassOf` axiom; also many profiles restrict universal
;;; quantification.
;;;
;;; I think the actual application of the profile-based restrictions will
;;; occur in the type-dispatch multimethods where the args are evaluated,
;;; but I think putting expressions and axioms into two different groups
;;; will be a nice cognitive aid, which can hopefully be parlayed into
;;; something user-facing as well.

(defmethod read-expression ::owl/AggregateExpression
  ([exp]
   (exp-t exp (fn [f args] ((type-dispatch f (first args))
                            (java.util.Set/copyOf args)))))
  ([exp onto]
   (exp-t exp (fn [f args] ((type-dispatch f (first args))
                            (java.util.Set/copyOf args))) onto)))

(defmethod read-expression ::owl/AggregateAxiom
  ([exp]
  (exp-t exp (fn [f args] ((type-dispatch f (first args))
                           (java.util.Set/copyOf args)))))
  ([exp onto]
  (exp-t exp (fn [f args] ((type-dispatch f (first args))
                           (java.util.Set/copyOf args))) onto)))

;; DONE: implement OWLLiteral types in `derive-entity-types` fn
;; and entity ctors mapping

;; TODO: fix, make use expression transformer
(defmethod read-expression ::owl/Facet [exp & [onto]]
  (let [facet (make-facet (first exp))
        lit (read-expression (second exp))]
    (.getOWLFacetRestriction fac facet lit)))

(defmethod read-expression ::owl/DatatypeRestriction
  ([exp]
  (exp-t exp (fn [f args] ((type-dispatch f (first args))
                           (first args)
                           (java.util.Set/copyOf (rest args))))))
  ([exp onto]
  (exp-t exp (fn [f args] ((type-dispatch f (first args))
                           (first args)
                           (java.util.Set/copyOf (rest args)))) onto)))

(defn read-annotation [a & [onto]]
  (make-annotation
   (make-annotation-property
    (make-iri (expand-owl-builtin (:property a) onto)))
   (read-literal (:value a))))

(defn read-axiom
  ([axiom]
  (let [[signature annotations] (vals axiom)]
    (cond
      (or
       (nil? annotations)
       (empty? annotations)) (read-expression signature)
      :else (.getAnnotatedAxiom
             (read-expression signature)
             (java.util.Set/copyOf
              (map read-annotation annotations))))))
  ([axiom onto]
   (let [[signature annotations] (vals axiom)]
     (cond
       (or
        (nil? annotations)
        (empty? annotations)) (read-expression signature onto)
       :else (.getAnnotatedAxiom
              (read-expression signature onto)
              (java.util.Set/copyOf
               (map read-annotation annotations)))))))


(defmacro read-owl
  ([owl-exp]
   `(reader/read-expression ~owl-exp))
  ([owl-exp onto]
   `(reader/read-expression ~owl-exp ~onto)))
  

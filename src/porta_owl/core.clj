(ns porta-owl.core
  (:require [porta-owl.ontology-managers :as om])
  (:require [porta-owl.utils :as utils])
  (:require [porta-owl.owl :as owl])
  (:require [porta-owl.reader :as reader])
  (:require [porta-owl.data-factory :as fac])
 ;; (:require [porta-owl.writer :as writer])
  (:use [porta-owl.datatypes])
  (:use [porta-owl.identifiers])
  (:import
   (org.swrlapi.factory SWRLAPIFactory)
   (org.semanticweb.owlapi.apibinding OWLManager)
   (org.semanticweb.owlapi.model OWLOntologyManager OWLOntology)
   (no.s11.owlapijsonld JsonLdParserFactory))
  (:gen-class))

(defn use-json-ld [] (JsonLdParserFactory/register))

(defn rule-engine [onto]
  (SWRLAPIFactory/createSWRLRuleEngine onto))

(defn asserted-axioms [engine] (.getAssertedOWLAxioms engine))
(defn inferred-axioms [engine] (.getInferredOWLAxioms engine))

(defn sqwrl-engine [onto]
  (SWRLAPIFactory/createSQWRLQueryEngine onto))

(defn sqwrl-query [engine q & [name]]
  (let [n (if (nil? name) (.toString (java.util.UUID/randomUUID))
              name)
        query (.createSQWRLQuery engine n q)]
    (fn [] (.runSQWRLQuery engine n q))))
;        query-name (str name)]
;    (intern ns (symbol name) (fn [] (.runSQWRLQuery engine name q)))))


(defn result-rows [result-mgr]
  (let [cols (.getColumnNames result-mgr)
        row_count (.getNumberOfRows result-mgr)]
    (into [] (map (fn [_] (if (true? (.next result-mgr))
                   (reduce
                    (fn [x y] 
                      (assoc x (keyword y) (.getValue result-mgr y)))
                    {}
                    cols)))
                  (range row_count)))))

(defn is-owl-exp? [exp]
  (if
      (and (coll? exp)
           (= "porta-owl.owl" (namespace (first exp))))
    true
    false))

(defn find-iri [has-iri]
  (cond
    (= org.semanticweb.owlapi.model.IRI (type has-iri)) has-iri
    (= java.lang.String (type has-iri)) (make-iri has-iri)
    (is-owl-exp? has-iri) (.getIRI (reader/read-owl has-iri))))

(defmacro annotation-assertions
  ([iri]
   `(let [onto-ns# '~iri]
      (.getAnnotationAssertionAxioms
       (onto-deref (symbol (namespace onto-ns#)))
       (find-iri ~iri))))
  ([onto iri]
   `(.getAnnotationAssertionAxioms ~onto (find-iri ~iri))))


(defn find-annotation [onto prop-iri subj-iri & xforms]
  (let [prop (make-iri (expand onto prop-iri))
        subj (make-iri (expand onto subj-iri))]
    (eduction
     (apply comp
            (filter #(= (.getIRI (.getProperty %)) prop))
            xforms)
     (.getAnnotationAssertionAxioms onto subj))))
  

(defn is-owl-obj? [x]
  (some #(= ::owl/OWLObject %) (ancestors (type x))))


(defn safe-owl-entity [x]
  (cond
    (is-owl-obj? x) x
    (string? x) (make-iri x)
    (coll? x) (reader/read-owl x)))


(defmacro get-ns-string [x]
  `(namespace '~x))

(defn- porta-label [onto label-iri entity]
  (let [[label alt-labels] (find-annotation
                            onto label-iri
                            entity (map #(.getLiteral (.getValue %))))
        iri (.getIRI entity)]
    (cond
      (empty? label) (apply
                      keyword
                      (map utils/make-kebab
                           [(clojure.string/replace
                             ((reduce-kv #(assoc %1 %3 %2) {}
                                         (prefix-map onto))
                              (.getNamespace iri))
                             #":" "")
                            (.getShortForm iri)]))
      :else (apply
             keyword
             (map utils/make-kebab
                  [(clojure.string/replace
                    ((reduce-kv #(assoc %1 %3 %2) {}
                                         (prefix-map onto))
                     (.getNamespace iri))
                    #":" "")
                   (clojure.string/replace
                    (.toString (.getValue (first (vals label))))
                    #"\"" "")])))))

(defn make-porta-label [onto entity]
  (porta-label onto "bungled" entity))
                                     
          

;;      (intern (symbol ~prefix) (symbol annotation-assertions)
;;             (partial annotation-assertions onto#))


(defn onto-deref [ns-sym]
  (deref (ns-resolve ns-sym 'ontology)))



;; (zoo/equivalent (owl/axiom [::owl/Class :zoo/critter] [::owl/Class :zoo/pet]))
;;; try using sigil sort of thing, ie.
;; (= [::owl/Class "stuff"] #c/"stuff"


(defmacro iri-macro [owl-exp]
  `(let [obj# (reader/read-expression ~owl-exp)]
     (get-iri obj#)))


(defmacro with-ontology [onto & body]
  `(doseq [exp# '~body]
     (eval (concat [(first exp#) '~onto] (rest exp#)))))
;  `(map (fn [x#] (eval (concat [(first x#) '~ns] (rest x#))))
;        '~body))

(defmacro onto-ns-sym [onto]
  `(symbol (namespace '~onto)))

(defn onto-intern [ns label val]
  (intern ns label val))


(defmacro defentity [onto label entity-exp & annotations]
  "Decl entity, adds it to ontology, returns ns-qualified entity coll"
  `(let [decl# (reader/read-expression
                [::owl/Declaration ~entity-exp])
         obj# (reader/read-expression ~entity-exp)]
     (om/add-axioms! ~onto [decl#])
     (if-let [atns# (vector ~@annotations)]
       (om/add-axioms! ~onto
                       (map  #(fac/make-annotation-assertion
                               (get-iri obj#)
                               (reader/read-annotation
                                % ~onto))
                             atns#))
       ;; else
       (om/add-axioms! ~onto
                       [(fac/make-annotation-assertion
                         (get-iri obj#)
                         (~reader/read-annotation
                          (~owl/annotation :rdfs/label
                           (str '~label))))]))1
     (intern (onto-ns-sym ~onto) '~label ~entity-exp)))
;;     (intern ~onto-ns '~label ~entity-exp)
;;     ~onto))

(defn contains-entity? [exp entity]
  "tests an expression signature for inclusion of an entity"
  (let* [sig (.getSignature exp)
         iri #{(find-iri (safe-entity entity))}
         entity-set (reduce #(clojure.set/union
                            %1 #{(.getIRI %2)})
                          #{} sig)]
    (boolean (some iri entity-set))))


;; (defmacro equivalent [clauses] `(owl/axiom [::owl/Equivalent ~@clauses]))

(defn defaxiom [sym axiom-kw]
  (let [f (fn [axiom-type onto & clauses]
            (om/add-axioms!
             onto
             [(reader/read-axiom
               (owl/axiom
                (reverse (reduce #(conj %1 %2)
                                 (flatten [axiom-type])
                                 clauses)))
               onto)]))]
    (intern *ns* sym (partial f axiom-kw))))

(defn intern-fn [ns sym f]
  (intern ns sym f))


(defaxiom 'equivalent ::owl/Equivalent)
(defaxiom 'disjoint ::owl/Disjoint)
(defaxiom 'same ::owl/Same)
(defaxiom 'assertion ::owl/Assertion)
(defaxiom 'subelement ::owl/SubElement)
(defaxiom 'subclass ::owl/SubClass)
(defaxiom 'subproperty ::owl/SubProperty)
(defaxiom 'declaration ::owl/Declaration)
(defaxiom 'negative-assertion ::owl/NegativeAssertion)
(defaxiom 'functional ::owl/Functional)
(defaxiom 'asymmetric ::owl/Asymmetric)
(defaxiom 'inverse ::owl/Inverse)
(defaxiom 'inverse-functional ::owl/InverseFunctional)
(defaxiom 'irreflexive ::owl/Irreflexive)
(defaxiom 'reflexive ::owl/Reflexive)
(defaxiom 'symmetric ::owl/Symmetric)
(defaxiom 'transitive ::owl/Transitive)
(defaxiom 'range ::owl/Range)
(defaxiom 'domain ::owl/Domain)

(def axiom-types
  ['equivalent
   'disjoint
   'same
   'assertion
   'subelement
   'subclass
   'subproperty
   'declaration
   'negative-assertion
   'functional
   'asymmetric
   'inverse
   'inverse-functional
   'irreflexive
   'reflexive
   'symmetric
   'transitive
   'range
   'domain])


(defmacro defontology [iri prefix & axioms]
  `(let [onto# (om/create-ontology ~iri)
         ns# (create-ns (symbol ~prefix))]
     (if (not (empty? (list ~@axioms)))
       (om/add-axioms! onto#
                       (map #(~reader/read-axiom %) (list ~@axioms))))
     (intern (symbol ~prefix) (symbol "ontology") onto#)
     (doseq [a# axiom-types]
       (intern-fn (symbol ~prefix) a# (partial (resolve a#) onto#)))
     (intern-fn (symbol ~prefix)
                (symbol "expand")
                (partial porta-owl.identifiers/expand onto#))
     (prefix-map-update! onto#
                         {":" ~iri ~prefix ~iri "_:" ""})))
  
;;(clojure.core/defn subelement [onto & clauses] (porta-owl.ontology-managers/add-axioms! onto [(porta-owl.reader/read-axiom (porta-owl.owl/axiom (clojure.core/reduce #(clojure.core/conj %1 %2) [:porta-owl.owl/SubElement] clauses)))]))

;; example:
;; (defontology "http://sammysvirtualzoo.com/ontology#" "zoo")
;; (zoo/assertion [::owl/Class "critter"] [::owl/NamedIndividual "soda"])

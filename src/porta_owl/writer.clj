(ns porta-owl.writer
  (:require [porta-owl.data-factory :as fac])
  (:require [porta-owl.reader :as reader])
  (:use [porta-owl.datatypes])
  (:use [porta-owl.identifiers])
  (:import
   (org.semanticweb.owlapi.model OWLOntologyManager OWLOntology IRI))
  (:gen-class))






;;(defn annotation-property [prop ]
;;  (


(defn map-annotation-assertions
  ([iri rf]
   (fn
     ([] )
     ([result] )
     ([result input] ))))


(defn find-annotation-assertions
;;  ([onto]
  ;;   (map (fn [iri] (.getAnnotationAssertionAxioms onto (safe-iri iri)))))
  ([onto] {})
  ([onto iri] {(keyword (.toString (safe-iri iri)))
               (.getAnnotationAssertionAxioms onto (safe-iri iri))})
  ([onto iri coll] (assoc
                    coll
                    (keyword (.toString (safe-iri iri)))
                    (.getAnnotationAssertionAxioms
                     onto (safe-iri iri)))))

(defn map-axioms [f coll]
  (map f coll))


(->> c
     (map (fn [x]
            (.getAnnotationAssertionAxioms
             homepage/ontology (writer/safe-iri x))))
     (filter #(not (empty? %)))
     (map #(filter (fn [x] (writer/axiom-contains? x :rdfs/label)) %)))



(defn make-xf [axiom-fn & f]
  (comp
   (map (fn [x] (axiom-fn x)))
   (filter #(not (empty %))) ))
;;   ((apply comp f))))


(defn axiom-contains? [a prop]
  (let* [sig (.getSignature a)
         iri #{(safe-iri (safe-property prop))}
         prop-set (reduce #(clojure.set/union
                            %1 #{(.getIRI %2)})
                          #{} sig)]
    (boolean (some iri prop-set))))

(def xf1 (comp
         (map #(get % :a))
         (map (fn [x]
                (.getAnnotationAssertionAxioms
                 homepage/ontology (writer/safe-iri x))))))

(def xf (comp
         (map (fn [x]
                (.getAnnotationAssertionAxioms
                 homepage/ontology (safe-iri x))))
         (map (fn [x] (.getAnnotationPropertiesInSignature x)))))
  
(defn get-annotation-assertions
  ([onto iri] (.getAnnotationAssertionAxioms onto (safe-iri iri)))
  ([onto iri property]
   (let [annotations (.getAnnotationAssertionAxioms
                      onto
                      (safe-iri iri))]
     (filter
      (fn [annotation]
        (boolean (some #{(safe-property property)}
         (reduce 
          #(into %1 (.toString (.getIRI %2)))
          #{}
          (.getAnnotationPropertiesInSignature annotation)))))
      annotations))))

(defn get-annotation-assertions2
  [onto results property]
  (->> (vals results)
       (map (fn [iri] (.getAnnotationAssertionAxioms onto (safe-iri iri))))
       (map (fn [prop] (.getAnnotationPropertiesInSignature prop)))
       (map (fn [prop] (reduce #(into %1 (.toString (.getIRI %2))) prop)))))


;; (defn filter-properties
;;   ([rf]
;;    (fn [] (rf))
;;    (fn [result] (rf result))
;;    (fn [result input] ))
;;   ([rf coll])
;;    ((filter-properties rf) #{} coll))
    

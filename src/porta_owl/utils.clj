(ns porta-owl.utils
  (:require [clojure.string :as str])
  (:gen-class))


(defn map-list-pairs
  "Turns a list of potential pairs into a key-value map."
  ([lst] (let [[i0 i1 & r] lst]
           (if (nil? r)
             {i0 i1}
             (map-list-pairs {i0 i1} r))))
  ([m lst] (let [[i0 i1 & r] lst]
             (if (nil? r)
               (assoc m i0 i1)
               (map-list-pairs (assoc m i0 i1) r)))))

(defn accept-map-or-list [& args]
  (let [[coll] args]
    (if (map? coll)
      (reduce map-list-pairs {} coll)
      (map-list-pairs args))))


(defn coll-consistent-type [coll]
  "Returns type of collection if collection is uniform type"
  (let [[x, r] (list (set coll))]
    (printf coll)
    (if (not (nil? r))
      (throw "script bungled")
      x)))

(defn kw->curie [kw]
  "Turns a keyword into a curie string"
  (str/replace-first (str kw) ":" ""))


(defn camel-to-kebab [s]
  (let [camel (str/replace
               s
               #"([A-Z]{1}(?![A-Z]+))"
               #(str/join ["-" (str/lower-case (%1 1))]))]
    (-> (subs camel 0 1)
        (case "-" (str/join (rest camel))
              camel)
        (str/replace #"((?<=[a-z])[A-Z]{1})"
                     #(str/join ["-" (str/lower-case (%1 1))]))
        (str/lower-case))))

(defn make-kebab [s]
  (-> (str/replace s #"_" "-")
      (camel-to-kebab)
      (str/replace " " "-")))

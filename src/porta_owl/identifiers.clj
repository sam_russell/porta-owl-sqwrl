(ns porta-owl.identifiers
  (:require [clojure.string :as str])
  (:import
   (org.semanticweb.owlapi.model OWLOntologyManager OWLOntology
                                 IRI OWLOntologyIRIMapper
                                 OWLLiteral))
  
  (:gen-class))

;; globals & constants

;; built-in OWL prefixes
(def prefixes {"rdf:"	"http://www.w3.org/1999/02/22-rdf-syntax-ns#"
               "rdfs:"	"http://www.w3.org/2000/01/rdf-schema#"
               "owl:"	"http://www.w3.org/2002/07/owl#"
               "xsd:"	"http://www.w3.org/2001/XMLSchema#"})

(defn make-iri 
  "Creates an OWLAPI IRI object"
  ([iri-string] (IRI/create iri-string))
  ([iri-string ns-string] (IRI/create ns-string iri-string)))

;; (defn safe-iri [has-iri]
;;   (condp = (type has-iri) 
;;     org.semanticweb.owlapi.model.IRI has-iri
;;     java.lang.String (IRI/create has-iri)
;;     clojure.lang.PersistentVector (.getIRI (reader/read-owl has-iri))
;;     (.getIRI has-iri)))


(defn get-iri
  ([owl-object] (.getIRI owl-object)))


(defn ontology-format [onto]
  "Returns ontology format instance"
  (let [mgr (.getOWLOntologyManager onto)]
    (.getOntologyFormat mgr onto)))

(defn prefix-map [onto]
  "Returns prefix map from ontology format instance"
  (cond
    (nil? onto) nil
    :else (into
           {}
           (.getPrefixName2PrefixMap
            (ontology-format onto)))))

(defn prefix-add! [onto curie-prefix full-iri-str]
  (let [prefix-mgr (ontology-format onto)]
    (.setPrefix prefix-mgr curie-prefix full-iri-str)))

(defn prefix-map-update! [onto new-prefix-map]
  "fake functional update prefix map"
  (let [to-add (filter
                #(not (contains? (prefix-map onto) %))
                new-prefix-map)]
    (doseq [prefix-pair to-add]
      (prefix-add! onto (first prefix-pair) (second prefix-pair)))
    (prefix-map onto)))
  

(defn is-full-iri? [identifier]
  "Determines if string is a full IRI based on crude regex"
  (not (nil? (re-find
              (re-pattern "^(?:http|https|uri|urn)://.+")
              identifier))))

(defn curie-ns-prefix [curie]
  ((str/split curie #":") 0))


(defn full-iri [identifier onto]
  "Turns a CURIE string into a full IRI."
  (cond
    (is-full-iri? identifier) identifier
    :else (str/join [(get (prefix-map onto)
                          (str/join [(curie-ns-prefix identifier) ":"]))
                     (subs identifier
                           (+ 1 (str/index-of identifier ":")))])))

(defn curie
  "Turns a full IRI string into a CURIE string."
  ([identifier]
   (let [full-iris (zipmap (vals prefixes) (keys prefixes))
         [qname localname] (str/split identifier
                                      (re-pattern "#") 0)]
     (if localname
       (str/join [(full-iris (str/join [qname "#"]))
                  localname])
       identifier)))
  ([identifier onto]
   (let* [mgr (ontology-format onto)
          prefixes (merge prefixes (prefix-map onto))
          full-iris (zipmap (vals prefixes) (keys prefixes))]
     (.getPrefixIRIIgnoreQName mgr (make-iri identifier)))))


(defn kw->iri [kw]
  (cond
    (string? kw) kw
    :else
    (let [n (str (namespace kw))
          i (last (clojure.string/split (str kw) #":"))]
      (clojure.string/replace i (str n "/") (str n #":")))))


(defn expand [onto kw-or-str]
  (let [iri (full-iri (kw->iri kw-or-str) onto)]
    (cond
      (and (= "" iri) (string? kw-or-str)) (clojure.string/join
                                            [":" kw-or-str])
      :else iri)))
  
